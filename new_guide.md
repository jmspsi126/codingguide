# PHP Coding Guideline
***
Coding Standards are an important factor for achieving a high code quality. 

A common visual style, naming conventions and other technical settings allow us to produce a homogenous code which is easy to read and maintain. 

However, not all important factors can be covered by rules and coding standards. 

Equally important is the style in which certain problems are solved programmatically - it's the personality and experience of the individual developer which shines through and ultimately makes the difference between technically okay code or a well considered, mature solution.

Thi guideline must be followed by everyone who creates code for script.

## Code Formatting and Layout aka "beautiful and good code"

- Easily read/understand each others code and consequently easily spot security problems or optimization opportunities
- It is a signal about consistency and cleanliness, which is a motivating factor for programmers striving for excellence

Some people may object to the visual guidelines since everyone has his own habits. 
Let's overcome this! 
The visual guideline must be followed along with coding guidelines for security. 
We want all contributions to the project to be as similar in style and as secure as possible.

## General considerations, Indentation and line formatting

1) Use [PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) and [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) coding standards.

- Code MUST use 4 spaces for indenting, not tabs.

- There MUST NOT be a hard limit on line length; the soft limit MUST be 120 characters; lines SHOULD be 80 characters or less.

- There MUST be one blank line after the namespace declaration, and there MUST be one blank line after the block of use declarations.

- Opening braces for classes MUST go on the next line, and closing braces MUST go on the next line after the body.

- Opening braces for methods MUST go on the next line, and closing braces MUST go on the next line after the body.

- Control structure keywords MUST have one space after them; method and function calls MUST NOT.

- Opening braces for control structures MUST go on the same line, and closing braces MUST go on the next line after the body.

- Opening parentheses for control structures MUST NOT have a space after them, and closing parentheses for control structures MUST NOT have a space before.

- No empty line between DocComment and class, member var or method.

2) Lines end with a newline a.k.a ``chr(10)`` - UNIX style

3) Files must be encoded in UTF-8 without byte order mark (BOM)

## Naming

- All developers choose cryptic abbreviations as naming.

- There are zillions of reasons for using proper names and in the end they all lead to better readable, manageable, stable and secure code.

- As a general note, english words (or abbreviations if necessary) must be used for all class names, method names, comments, variables names, database table and field names. 

- When using abbreviations or acronyms remember to make them camel-cased as needed, no all-uppercase stuff.

1) Vendor namespaces and Package names

- Namespace starts with vendor name followed by package key (name) and subparts as needed

ex:``namespace Test\TestPackage;``.Test is vendor namespace 
Why do we use vendor namespaces? 
We don't need a central package key registry and secondly, it allows anyone to seamlessly integrate third-party
packages, such as Symfony2 components and Zend Framework components or virtually any other PHP library.

- All package names start with an uppercase character.
  In order to avoid problems with different filesystems,only the characters a-z, A-Z, 0-9 and the dash sign "-" are allowed for package names – don't use special characters.
  
2) Namespace and Class names

- Class names should be nouns.
- Only the characters a-z, A-Z and 0-9 are allowed for namespace and class names.
- Namespaces are usually written in UpperCamelCase but variations are allowed for well
  established names and abbreviations.
- Class names are always written in ``UpperCamelCase``.
- The unqualified class name must be meant literally even without the namespace.
- The main purpose of namespaces is categorization and ordering
- Class names must be nouns, never adjectives.
- The name of test classes must start with the word "Test", class names of task
  must end with the word "Task".
  
3) Importing Namespaces	

- If you refer to other classes or interfaces you are encouraged to import the namespace with the use statement if it improves readability.
- If importing namespaces creates conflicting class names you might alias class/interface or namespaces with the as keyword.
- One use statement per line, one use statement for each imported namespace.
- Imported namespaces should be ordered alphabetically.
- Don't import namespaces unless you use them.

ex: ``use Task\TestPackage\Service\TaskManager;``.``use Task\Test\Annotations as Task;``

3) Interface names and Exception names

- Only the characters a-z, A-Z and 0-9 are allowed for interface names – don't use special characters.
  All interface names are written in ``UpperCamelCase``. 
  Interface names must be adjectives or nouns and have the Interface suffix.
  ex: ``Test\TestManagement\ObjectInterface``
- There are two possible types of exceptions.
  Generic exceptions should be named "Exception" preceded by their namespace. 
  Specific exceptions should reside in their own sub-namespace end with the word ``Exception``.
  ex: ``\Test\TestManagement\Exception`` . ``\Test\TestManagement\Exception\InvalidClassNameException``

4) Method names

- All method names are written in lowerCamelCase. 
- In order to avoid problems with different filesystems, only the characters a-z, A-Z and 0-9 are allowed for method names – don’t use special characters.
- Make method names descriptive, but keep them concise at the same time. 
- Constructors must always be called __construct(), never use the class name as a method name.
  ex: ``someTestMethodName()`` . ``__construct()``
  
5) Variable names

- Variable names are written in lowerCamelCase and should be
  ~ self-explanatory
  ~ not shortened beyond recognition, but rather longer if it makes their meaning clearer.
  ex: Correct naming of variable : ``$singletonObjectsRegistry``
      Incorrect naming of variable : ``$$cx``
  ~ use variable names like $i, $j and $k for numeric indexes in for loops if it’s clear what they mean on the first sight.
  ex: ``for($i=1:i<10:i++)``
  
6) Constant names

- All constant names are written in UPPERCASE. This includes TRUE, FALSE and NULL. Words can be separated by underscores - you can also use the underscore to group constants thematically:
  ``STUFF_LEVEL``
  ``COOLNESS_FACTOR``
  ``PATTERN_MATCH_EMAILADDRESS``
  ``PATTERN_MATCH_VALIDHTMLTAGS``
- It is, by the way, a good idea to use constants for defining regular expression patterns (as seen above) instead of defining them somewhere in your code.

7) Filenames

- All filenames are UpperCamelCase.
- Class and interface files are named according to the class or interface they represent
- Each file must contain only one class or interface
- Names of files containing code for unit tests must be the same as the class which is tested, appended with “Test.php”.
- Files are placed in a directory structure representing the namespace structure. 


## PHP code formatting

1) We follow the [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) standard which is defined by PHP FIG.

Please check [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md).

2) Some things are not specified in PSR-2, so here are some amendments.

In general, we use single quotes to enclose literal strings:
 ``$test = 'This is test project';``
 
 If you’d like to insert values from variables, concatenate strings. A space must be inserted before and after the dot for better readability:
 
 ``$message = 'Hello ' . $name . ', you look ' . $appearance . ' today!';``
 
 You may break a string into multiple lines if you use the dot operator. You’ll have to indent each following line to mark them as part of the value assignment:
 
 ``$neos = 'A big ' .
 
  'project from ' .
  
  'a test ' .
  
  'team';``
  
3) Multiline conditions: Indent them and add a extra indent to following code.
 Put the boolean operators at beginning of line.
	 ``if (isset($someArray['question'])
	 && $this->answerToEverything === 42
	 || count($someArray) > 3 {
	 $this->fanOfFlow = true;
	 }``

## Add comment on PHP code.

1) Capture the joy of coding as you create excellent web solutions. Enjoy coding.

- Every file must contain a header stating namespace and licensing information
- Declare your namespace.
- The copyright header itself must not start with /**, as this may confuse documentation generators!


	 
2) Description of the class.

-  Make it as long as needed, feel free to explain how to use it.
	
- Only use inline @var annotations when type can't be derived (like in an array of objects) to increase readability and trigger IDE autocompletion.
 
